import * as React from "react";
import withStyles, { InjectedProps, InputSheet } from "react-typestyle";
import { darkGrey, white } from "../styles/colors";
import { em, percent } from "csx";
import NoSleepJS = require("nosleep.js");

const isIOS =
  !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

export interface INoSleepState {
  enabled: boolean;
}

class NoSleepComponent extends React.PureComponent<
  InjectedProps,
  INoSleepState
> {
  public static styles: InputSheet<{}> = {
    root: {
      backgroundColor: white.toString(),
      border: "none",
      borderRadius: em(0.2),
      color: darkGrey.toString(),
      cursor: "pointer",
      fontWeight: "bold",
      left: percent(50),
      padding: em(0.5),
      position: "fixed",
      top: em(1),
      transform: "translateX(-50%)",
      zIndex: 1,
    },
  };

  private nosleep: NoSleepJS;

  public constructor(props: InjectedProps) {
    super(props);

    this.nosleep = new NoSleepJS();

    this.state = {
      enabled: window.location.search.indexOf("app") !== -1,
    };
  }

  public componentWillUnmount() {
    if (this.state.enabled) {
      this.nosleep.disable();
    }
  }

  public render() {
    const { classNames } = this.props;
    const { enabled } = this.state;
    return (
      !enabled && (
        <button
          type="button"
          className={classNames.root}
          onClick={this.boundEnable}
        >
          {isIOS ? (
            <span>Click here to keep your screen awake!</span>
          ) : (
            <span>
              Click here to enable fullscreen and keep your screen awake!
            </span>
          )}
        </button>
      )
    );
  }

  private boundEnable = () => this.enable();
  private enable() {
    this.nosleep.enable();
    this.setState({ enabled: true });

    // Try full screen the device. Doesn't work on ios
    const doc: any = document.documentElement;
    const rfs =
      doc.requestFullscreen ||
      doc.webkitRequestFullScreen ||
      doc.mozRequestFullScreen ||
      doc.msRequestFullscreen;
    rfs.call(doc);
  }
}

export const NoSleep = withStyles()<{}>(NoSleepComponent);
