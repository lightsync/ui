import * as React from "react";
import withStyles, { InjectedProps, InputSheet } from "react-typestyle";
import { centerCenter } from "csstips";
import { em, percent } from "csx";
import { ISequencePlayerState } from "./SequencePlayer";
import { start } from "repl";
import { SyncService } from "../lib/SyncService";

export interface ISequenceProps {
  name: string;
  sequence: ISequenceKeyframe[];
  playerState: ISequencePlayerState;
  syncService: SyncService;
  startTimestamp?: number;
}

export interface ISequenceState {
  keyframe: ISequenceKeyframeChild;
}

export interface ISequenceKeyframeChild {
  className: string;
  contentClassName?: string;
  duration: number;
  text?: string;
  textFn?: (playerState: ISequencePlayerState) => string;
}

export interface ISequenceKeyframe {
  repeat: number;
  children: ISequenceKeyframeChild[];
}

type TCombinedProps = InjectedProps & ISequenceProps;

class SequenceComponent extends React.PureComponent<
  TCombinedProps,
  ISequenceState
> {
  public static styles: InputSheet<{}> = {
    content: {
      maxWidth: em(20),
    },
    root: {
      ...centerCenter,
      boxSizing: "border-box",
      display: "flex",
      height: percent(100),
      padding: em(1),
      width: percent(100),
    },
  };

  private destroyed = false;
  private handle?: number;

  public constructor(props: TCombinedProps) {
    super(props);

    this.state = {
      keyframe: props.sequence[0].children[0],
    };
  }

  public componentDidMount() {
    this.tick();
  }

  public componentWillUnmount() {
    this.destroyed = true;
  }

  public render() {
    const { classNames, playerState } = this.props;
    const keyframe = this.state.keyframe as ISequenceKeyframeChild;
    let text = "";

    if (keyframe.textFn) {
      text = keyframe.textFn(playerState);
    } else if (keyframe.text) {
      text = keyframe.text;
    }

    if (!this.handle) {
      this.handle = window.requestAnimationFrame(this.tick);
    }

    return (
      <div className={[classNames.root, keyframe.className].join(" ")}>
        <div
          className={keyframe.contentClassName || classNames.content}
          dangerouslySetInnerHTML={{ __html: text }}
        />
      </div>
    );
  }

  private tick = () => {
    if (this.destroyed) {
      return;
    }

    const { syncService } = this.props;
    const offset = syncService.getStartTimestampDiff();

    if (!offset) {
      return;
    }

    const timeline = this.props.sequence;
    const timelineLength = timeline.length;

    let currentKeyframe: ISequenceKeyframeChild | undefined;
    let currentOffset = 0;

    for (let i = 0; i < timelineLength; i++) {
      const parent = timeline[i];

      for (let j = 0; j < parent.repeat; j++) {
        const childrenLength = parent.children.length;

        for (let k = 0; k < childrenLength; k++) {
          const keyframe = parent.children[k];
          currentOffset += keyframe.duration;

          if (offset < currentOffset) {
            currentKeyframe = keyframe;
            break;
          }
        }

        if (currentKeyframe) {
          break;
        }
      }

      if (currentKeyframe) {
        break;
      }
    }

    if (!currentKeyframe) {
      const children = timeline[timelineLength - 1].children;
      currentKeyframe = children[children.length - 1];
      this.handle = undefined;
    } else {
      this.handle = window.requestAnimationFrame(this.tick);
    }

    if (currentKeyframe && this.state.keyframe !== currentKeyframe) {
      this.setState({ keyframe: currentKeyframe });
    }
  };
}

export const Sequence = withStyles()<ISequenceProps>(SequenceComponent);
