import * as React from "react";
import withStyles, { InjectedProps, InputSheet } from "react-typestyle";
import { centerCenter } from "csstips";
import { darkGrey, white } from "../styles/colors";
import { em, percent, viewHeight } from "csx";
import { IMessageStartTimestampPayload, SyncService } from "../lib/SyncService";
import { ISequenceKeyframe, Sequence } from "./Sequence";
import moment = require("moment");

export interface ISequenceMap {
  [name: string]: ISequenceKeyframe[];
}

export interface ISequencePlayerProps {
  sequences: ISequenceMap;
  syncService: SyncService;
}

export interface ISequencePlayerState {
  name: string;
  days: number;
  asHours: number;
  hours: number;
  minutes: number;
  seconds: number;

  startTimestamp?: number;
  syncing: boolean;
}

class SequencePlayerComponent extends React.PureComponent<
  InjectedProps & ISequencePlayerProps,
  ISequencePlayerState
> {
  public static styles: InputSheet<{}> = {
    content: {
      boxSizing: "border-box",
      maxWidth: em(22),
      padding: em(1),
    },
    root: {
      ...centerCenter,
      backgroundColor: darkGrey.toString(),
      boxSizing: "border-box",
      color: white.toString(),
      display: "flex",
      fontSize: em(2),
      height: viewHeight(100),
      width: percent(100),
    },
  };

  private timeout?: number;

  public constructor(props: InjectedProps & ISequencePlayerProps) {
    super(props);

    this.state = {
      asHours: 0,
      days: 0,
      hours: 0,
      minutes: 0,
      name: props.syncService.getName() || "",
      seconds: 0,

      startTimestamp: props.syncService.getStartTimestamp(),
      syncing: true,
    };
  }

  public componentDidMount() {
    this.props.syncService.on("startTimestamp", this.onStartTimestamp);
    this.tick();
  }

  public componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.props.syncService.removeListener(
      "startTimestamp",
      this.onStartTimestamp,
    );
  }

  public render() {
    const { classNames, sequences, syncService } = this.props;
    const { name, startTimestamp, syncing } = this.state;
    const keyframes: ISequenceKeyframe[] = sequences[name] || [];

    return (
      <div className={classNames.root}>
        {syncing ? (
          <div className={classNames.content}>Syncing...</div>
        ) : (
          <Sequence
            syncService={syncService}
            playerState={this.state}
            sequence={keyframes}
            name={name}
            startTimestamp={startTimestamp}
          />
        )}
      </div>
    );
  }

  private tick() {
    const { syncService } = this.props;
    const diff = syncService.getStartTimestampDiff();

    if (typeof diff === "number") {
      const duration = moment.duration(-1 * diff).subtract(10, "minutes");

      if (duration.asMilliseconds() < 0) {
        this.setState({
          asHours: 0,
          days: 0,
          hours: 0,
          minutes: 0,
          seconds: 0,
          syncing: false,
        });
      } else {
        this.setState({
          asHours: Math.floor(duration.asHours()),
          days: duration.days(),
          hours: duration.hours(),
          minutes: duration.minutes(),
          seconds: duration.seconds(),
          syncing: false,
        });
      }
    } else {
      this.setState({
        syncing: true,
      });
    }
    this.timeout = window.setTimeout(() => this.tick(), 1000);
  }

  private onStartTimestamp = (payload: IMessageStartTimestampPayload) => {
    const { syncService } = this.props;

    this.setState({
      name: payload.name,
      startTimestamp: syncService.getStartTimestamp(),
      syncing: !syncService.isSynced(),
    });
  };
}

export const SequencePlayer = withStyles()<ISequencePlayerProps>(
  SequencePlayerComponent,
);
