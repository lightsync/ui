import { create, ITimesyncClient } from "timesync";
import { EventEmitter } from "events";
import ReconnectingWebsocket from "reconnecting-websocket";
import io = require("socket.io-client");

export type IMessageType = "START_TIMESTAMP";

export interface IMessage {
  type: IMessageType;
  payload: IMessageStartTimestampPayload;
}

export interface IMessageStartTimestampPayload {
  name: string;
  timestamp: number;
}

export class SyncService extends EventEmitter {
  private ws: ReconnectingWebsocket;
  private socket: SocketIOClient.Socket;
  private ts: ITimesyncClient;
  private cueName?: string;
  private startTimestamp?: number;
  private gotOffset = false;

  public constructor(opts: { timesyncURI: string; cueWSURI: string }) {
    super();

    this.socket = io(opts.timesyncURI, {
      transports: ["websocket"],
    });
    this.ts = this.setupTimesync();

    // Setup websocket to sync start time
    this.ws = new ReconnectingWebsocket(opts.cueWSURI);
    this.ws.onmessage = this.onMessage;
  }

  public getName() {
    return this.cueName;
  }

  public isSynced() {
    return this.gotOffset;
  }

  public getStartTimestamp() {
    return this.startTimestamp;
  }

  public getStartTimestampDiff() {
    if (!this.startTimestamp || !this.gotOffset) {
      return;
    }

    return this.ts.now() - this.startTimestamp;
  }

  private onMessage = (msg: MessageEvent) => {
    const payload: IMessage = JSON.parse(msg.data);

    switch (payload.type) {
      case "START_TIMESTAMP":
        this.onStartTimestamp(payload);
    }
  };

  private onStartTimestamp(msg: IMessage) {
    const { name, timestamp } = msg.payload;
    // tslint:disable no-console
    this.cueName = name;
    this.startTimestamp = timestamp;
    console.log(
      "SyncService.onStartTimestamp",
      name,
      timestamp,
      this.ts.offset,
    );

    this.emit("startTimestamp", msg.payload);
  }

  private setupTimesync() {
    const ts = create({
      interval: 20000,
      server: this.socket,
    });
    ts._isFirst = false;

    ts.on("change", offset => {
      this.gotOffset = true;
      console.log("timesync change:", offset);
    });

    ts.send = (socket, data, timeout) => {
      return new Promise((resolve, reject) => {
        const timeoutFn = setTimeout(reject, timeout);
        socket.emit("timesync", data, () => {
          clearTimeout(timeoutFn);
          resolve();
        });
      });
    };

    this.socket.on("timesync", (data: any) => {
      ts.receive(null, data);
    });

    return ts;
  }
}
