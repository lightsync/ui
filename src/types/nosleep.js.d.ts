declare module "nosleep.js" {
  class NoSleep {
    public enable(): any;
    public disable(): any;
  }

  export = NoSleep;
}
