import { color } from "csx";

export const white = color("#ffffff");
export const darkGrey = color("#333333");
export const black = color("#000000");
